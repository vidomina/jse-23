package com.ushakova.tm.repository;

import com.ushakova.tm.api.IRepository;
import com.ushakova.tm.model.AbstractEntity;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final List<E> entities = new ArrayList<>();

    @Override
    @NotNull
    public E add(final @NotNull E entity) {
        entities.add(entity);
        return entity;
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @Override
    @NotNull
    public List<E> findAll() {
        return entities;
    }

    @Override
    @Nullable
    public E findById(final @NotNull String id) {
        return entities.stream()
                .filter(e -> e.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void remove(final @NotNull E entity) {
        entities.remove(entity);
    }

    @Override
    @Nullable
    public E removeById(final @NotNull String id) {
        @Nullable final E entity = findById(id);
        Optional.ofNullable(entity).ifPresent(entities::remove);
        return entity;
    }

}
