package com.ushakova.tm.bootstrap;

import com.ushakova.tm.api.repository.ICommandRepository;
import com.ushakova.tm.api.repository.IProjectRepository;
import com.ushakova.tm.api.repository.ITaskRepository;
import com.ushakova.tm.api.repository.IUserRepository;
import com.ushakova.tm.api.service.*;
import com.ushakova.tm.command.AbstractCommand;
import com.ushakova.tm.command.auth.*;
import com.ushakova.tm.command.project.*;
import com.ushakova.tm.command.system.*;
import com.ushakova.tm.command.task.*;
import com.ushakova.tm.command.user.*;
import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.exception.system.UnknownCommandException;
import com.ushakova.tm.repository.CommandRepository;
import com.ushakova.tm.repository.ProjectRepository;
import com.ushakova.tm.repository.TaskRepository;
import com.ushakova.tm.repository.UserRepository;
import com.ushakova.tm.service.*;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthService authService = new AuthService(userService);

    {
        registry(new TaskFindByIdCommand());
        registry(new TaskFindByIndexCommand());
        registry(new TaskFindByNameCommand());
        registry(new TaskUpdateTaskByIdCommand());
        registry(new TaskUpdateTaskByIndexCommand());
        registry(new TaskStartTaskByIdCommand());
        registry(new TaskStartTaskByIndexCommand());
        registry(new TaskStartTaskByNameCommand());
        registry(new TaskCompleteTaskByIdCommand());
        registry(new TaskCompleteTaskByIndexCommand());
        registry(new TaskCompleteTaskByNameCommand());
        registry(new TaskChangeStatusByNameCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskShowListCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectFindByIdCommand());
        registry(new ProjectFindByIndexCommand());
        registry(new ProjectFindByNameCommand());
        registry(new ProjectUpdateProjectByIdCommand());
        registry(new ProjectUpdateProjectByIndexCommand());
        registry(new ProjectStartProjectByIdCommand());
        registry(new ProjectStartProjectByIndexCommand());
        registry(new ProjectStartProjectByNameCommand());
        registry(new ProjectCompleteProjectByIdCommand());
        registry(new ProjectCompleteProjectByIndexCommand());
        registry(new ProjectCompleteProjectByNameCommand());
        registry(new ProjectChangeStatusByNameCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectShowListCommand());
        registry(new TaskBindByProjectIdCommand());
        registry(new TaskUnbindFromProjectCommand());

        registry(new AuthLoginCommand());
        registry(new AuthLogoutCommand());
        registry(new AuthRegistryCommand());
        registry(new AuthShowProfileInfoCommand());
        registry(new AuthUpdateProfileCommand());
        registry(new AuthSetPasswordCommand());
        registry(new UserFindByIdCommand());
        registry(new UserFindByLoginCommand());
        registry(new UserRemoveByIdCommand());
        registry(new UserRemoveByLoginCommand());
        registry(new UserShowListCommand());
        registry(new UserLockByLoginCommand());
        registry(new UserUnlockByLoginCommand());

        registry(new AboutCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new SystemInfoCommand());
        registry(new VersionCommand());
        registry(new ArgumentsListCommand());
        registry(new CommandsListCommand());
    }

    @Override
    @NotNull
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    @NotNull
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    @NotNull
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public @NotNull IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    @NotNull
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    @NotNull
    public IUserService getUserService() {
        return userService;
    }

    private void initUsers() {
        getUserService().add("Guest", "Guest", "wholah0@baidu.com");
        getUserService().add("Admin", "Admin", Role.ADMIN);
    }

    public void parseArg(@Nullable final String arg) {
        if (!Optional.ofNullable(arg).isPresent() || arg.isEmpty()) return;
        @Nullable final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) return;
        @Nullable final Role[] roles = command.roles();
        authService.checkRoles(roles);
        command.execute();
    }

    public boolean parseArgs(@Nullable String[] args) {
        if (!Optional.ofNullable(args).isPresent() || args.length == 0) return false;
        @Nullable final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void parseCommand(@Nullable final String cmd) {
        if (!Optional.ofNullable(cmd).isPresent() || cmd.isEmpty()) return;
        @Nullable final AbstractCommand command = commandService.getCommandByName(cmd);
        if (!Optional.ofNullable(command).isPresent()) throw new UnknownCommandException(cmd);
        @Nullable final Role[] roles = command.roles();
        authService.checkRoles(roles);
        command.execute();
    }

    private void registry(@Nullable final AbstractCommand command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(@Nullable final String... args) {
        loggerService.debug("<<Debug Message>>");
        loggerService.info("*   Welcome To Task Manager   *");
        if (parseArgs(args)) System.exit(0);
        initUsers();
        while (true) {
            System.out.println("***Enter Command: ");
            @Nullable final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try {
                parseCommand(command);
                System.out.println("[Ok]");
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[Fail]");
            }
        }
    }

}
