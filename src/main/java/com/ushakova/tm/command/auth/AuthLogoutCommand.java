package com.ushakova.tm.command.auth;

import com.ushakova.tm.command.AbstractUserCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class AuthLogoutCommand extends AbstractUserCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Logout.";
    }

    @Override
    public void execute() {
        System.out.println("Logout");
        serviceLocator.getAuthService().logout();
    }

    @Override
    @NotNull
    public String name() {
        return "logout";
    }

}
