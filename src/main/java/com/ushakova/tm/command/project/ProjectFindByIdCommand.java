package com.ushakova.tm.command.project;

import com.ushakova.tm.command.AbstractProjectCommand;
import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.model.Project;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectFindByIdCommand extends AbstractProjectCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Show project by id.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("***Show Project***\nEnter Id:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final Project project = serviceLocator.getProjectService().findById(id);
        showProjectInfo(project);
    }

    @Override
    @NotNull
    public String name() {
        return "project-view-by-id";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
