package com.ushakova.tm.model;

import com.ushakova.tm.enumerated.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


@Getter
@Setter
@NoArgsConstructor
public class User extends AbstractEntity {

    @NotNull
    private String login;

    @NotNull
    private String passwordHash;

    @NotNull
    private String email;

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    @NotNull
    private Boolean locked = false;

    @NotNull
    private Role role = Role.USER;

    @Override
    @NotNull
    public String toString() {
        return "Id: " + this.getId()
                + "\nFirst Name: " + firstName
                + "\nMiddle Name: " + middleName
                + "\nLast Name: " + lastName
                + "\nEmail: " + email
                + "\nLogin: " + login
                + "\nRole: " + role.getDisplayName();
    }

}
