package com.ushakova.tm.exception.empty;

import com.ushakova.tm.exception.AbstractException;

public class EmptyDescriptionException extends AbstractException {

    public EmptyDescriptionException() {
        super("An error has occurred: id is empty.");
    }

}
